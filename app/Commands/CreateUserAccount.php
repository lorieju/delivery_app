<?php namespace App\Commands;

use App\Commands\Command;

use App\Http\Requests\CreateUserRequest;
use App\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateUserAccount extends Command implements SelfHandling {

    protected $first_name;
    protected $last_name;
    protected $email;
    protected $mobile;
    protected $password;
    protected $type;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(CreateUserRequest $request)
	{
        $this->first_name = $request->get('first_name');
        $this->last_name = $request->get('last_name');
        $this->email = $request->get('email');
        $this->mobile = $request->get('mobile');
        $this->password = $request->get('password');
        $this->type = $request->get('type');
    }

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$user = new User([
            'email' => $this->email,
            'password' => bcrypt($this->password)
        ]);
        $user->save();
        $profile = new Profile([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'mobile' => $this->mobile,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $user->profile()->save($profile);

        $user->roles()->attach($this->type);

	}

}
