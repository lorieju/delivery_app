<?php namespace App\Commands;

use App\Commands\Command;

use App\Http\Requests\ChangePasswordRequest;
use App\User;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\Auth;

class ChangePassword extends Command implements SelfHandling {

    public $new_password;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ChangePasswordRequest $request)
	{
        $this->new_password = $request->get('new_password');
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
        $user = User::with(['profile'])->where('id','=',Auth::id())->first();

        $user->password = bcrypt($this->new_password);
        $user->save();
	}

}
