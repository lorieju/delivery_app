<?php namespace App\Commands;

use App\Commands\Command;

use App\Http\Requests\CreatePizzaRequest;
use App\Pizza;
use App\PizzaSize;
use Illuminate\Contracts\Bus\SelfHandling;

class CreatePizza extends Command implements SelfHandling {

    public $title;
    public $description;
    public $file_name;
    public $size;
    public $price;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(CreatePizzaRequest $request,$file_name)
	{
		$this->title = $request->get('title');
		$this->description = $request->get('description');
		$this->file_name = $file_name;
		$this->size = $request->get('size');
		$this->price = $request->get('price');
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$pizza = new Pizza([
            'name' => $this->title,
            'description' => $this->description,
            'image' => $this->file_name
        ]);
        $pizza->save();
        $pizza_size = [];
        foreach($this->size as $skey => $size){
            foreach($this->price as $pkey => $price){
                if($skey == $pkey){
                    $pizza_size[] = new PizzaSize(['size' => $size,'price' => $price]);
                }
            }
        }
        $pizza->pizzaSize()->saveMany($pizza_size);
	}

}
