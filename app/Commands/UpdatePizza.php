<?php namespace App\Commands;

use App\Commands\Command;

use App\Http\Requests\Request;
use App\Http\Requests\UpdatePizzaRequest;
use App\Pizza;
use App\PizzaSize;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdatePizza extends Command implements SelfHandling {

    public $title;
    public $description;
    public $id;
    public $size;
    public $price;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(UpdatePizzaRequest $request)
	{
        $this->id = $request->get('id');
		$this->title = $request->get('title');
		$this->description = $request->get('description');
		$this->size = $request->get('size');
		$this->price = $request->get('price');
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
        $pizza = Pizza::where('id', '=', $this->id)->with(['pizzaSize'])->firstOrFail();

        $pizza->name = $this->title;
        $pizza->description = $this->description;
        $pizza->save();

        $pizza_size = [];
        $size_array = $this->size;
        $price_array = $this->price;

        foreach($size_array as $skey => $size){
            foreach($price_array as $pkey => $price){
                if($skey == $pkey){
                    $pizza_size[] = new PizzaSize(['size' => $size,'price' => $price]);
                }
            }
        }

        $pizza->pizzaSize()->delete();
        $pizza->pizzaSize()->saveMany($pizza_size);
	}

}
