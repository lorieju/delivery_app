<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/modal_request/{id}', 'WelcomeController@modal_request');

Route::group(['middleware' => 'auth'], function(){

    Route::post('upload', 'GenericController@upload');

    Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function(){
        Route::get('/dashboard/pizza-management', 'PizzaController@manage');
        Route::get('/dashboard/pizza-add', 'PizzaController@add');
        Route::get('/dashboard/pizza-edit/{id}', 'PizzaController@edit');
        Route::get('/dashboard/pizza-delete/{id}', 'PizzaController@requestDelete');
        Route::post('/pizza_delete', 'PizzaController@delete');
        Route::post('/pizza', 'PizzaController@create');
        Route::put('/pizza', 'PizzaController@update');
        Route::get('/dashboard/user','ProfileController@allUser');
        Route::get('/dashboard/add_user','ProfileController@addUser');
        Route::post('/add_user','ProfileController@store');
        Route::get('/account_type','ProfileController@requestAccountType');
        Route::post('/account_type','ProfileController@AccountType');
        Route::post('/order_update','BookingController@orderUpdate');
        Route::get('/order_update','BookingController@requestUpdate');
        Route::get('/order_delete','BookingController@viewOrderDelete');
        Route::post('/order_delete','BookingController@orderDelete');
    });

    Route::group(['prefix' => 'dashboard'], function(){
        Route::get('/','BackendController@index');
        Route::get('/pizza', 'PizzaController@index');
        Route::post('/purchase', 'PurchaseController@create');
        Route::get('/order','BookingController@orders');
    });

    Route::group(['prefix' => 'profile'], function(){
        Route::get('/','ProfileController@index');
        Route::get('/edit','ProfileController@edit');
        Route::post('update', 'ProfileController@update');
        Route::post('change_password', 'ProfileController@change_password');
    });

    Route::post('/booking','BookingController@index');
    Route::get('/order_view','BookingController@viewOrder');
    Route::post('/purchase','PurchaseController@create');
    Route::get('/view_invoice', 'BookingController@invoiceView');
});




Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
