<?php namespace App\Http\Middleware;

use Closure;

class AdminMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        $roles = $request->user()->roles->lists('name');

        if(in_array('customer', $roles))
        {
            return redirect('dashboard');
        }

        return $next($request);
	}

}
