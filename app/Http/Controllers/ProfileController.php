<?php namespace App\Http\Controllers;

use App\Commands\ChangePassword;
use App\Commands\CreateUserAccount;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$crumbs = 'Profile';

        return view('backend.profile.index',compact('crumbs'));
	}

	public function requestAccountType(Request $request)
    {
        $id = $request->get('user');

        $user = User::with(['profile'])->where('id','=', $id)->firstOrFail();

        return view('backend.user._account_type',compact('user'));
    }

    public function AccountType(Request $request)
    {
        $id = $request->get('id');

        $user = User::with(['profile'])->where('id','=', $id)->firstOrFail();
        $user->active = $request->get('status');
        $user->save();

        $name = ucwords($user->profile->first_name.' '.$user->profile->last_name);
        return Redirect::back()->with([
            'alert-level' => 'success',
            'message' => 'User '. $name .' successfully changed status.'
        ]);
    }

    public function addUser()
    {
        $crumbs = 'Add user';

        return view('backend.user.add',compact('crumbs'));
    }

    public function store(CreateUserRequest $request)
    {
        $name = ucwords($request->get('first_name') .' '. $request->get('last_name'));

        $this->dispatch(new CreateUserAccount($request));

        return redirect('dashboard/user')->with([
            'alert-level' => 'success',
            'message' => 'User '. $name .' successfully added.'
        ]);
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
        $crumbs = 'Profile Edit';

        return view('backend.profile.edit',compact('crumbs'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UpdateProfileRequest $request)
	{
        $user = User::with(['profile'])->where('id','=',Auth::id())->firstOrFail();

        User::doUpdate(Auth::user()->id,$request);

        return redirect('profile')->with([
            'alert-level' => 'success',
            'message' => 'Profile has been successfully updated.'
        ]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function change_password(ChangePasswordRequest $request)
    {
        if ($this->passwordMatched($request))
        {
            $this->dispatch(new ChangePassword($request));
        }
        else
        {
            return redirect()->back()->with([
                'alert-level' => 'error',
                'message' => 'Current password credentials do not match our records.'
            ]);
        }

        return redirect('profile')->with([
            'alert-level' => 'success',
            'message' => 'Password changed successfully.'
        ]);
    }

    public function allUser(){
        $crumbs = 'Users';
        $users = User::fetchAll();
        return view('backend.user.index',compact('crumbs','users'));
    }

    private function passwordMatched(Request $request)
    {
        $password = $request->get('current_password');

        $user = User::where('id','=',Auth::id())->first();

        if (Hash::check($password, $user->password))
        {
            return true;
        } else {
            return false;
        }
    }
}
