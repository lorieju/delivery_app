<?php namespace App\Http\Controllers;

use App\Pizza;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $pizzas = Pizza::fetchAll();

		return view('frontend.index',compact('pizzas'));
	}

    public function modal_request($id)
    {
        $pizza = Pizza::with(['pizzaSize'])->where('id','=',$id)->firstOrFail();

        return view('frontend.modal._modal_request',compact('pizza'));
    }

}
