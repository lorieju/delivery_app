<?php namespace App\Http\Controllers;

use App\Events\OrderHasConfirmed;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Pizza;
use App\Purchase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{

    public function index(Request $request)
    {
        $booking = $request->get('data');
        $pizzaIds = array();
        $pizzaSize = array();
        foreach ($booking as $book) {
            $pizzaIds[] = $book['pizzaId'];
            $pizzaSize[] = $book['pizzaSize'];
            $quantity[] = $book['quantity'];
        }

        Session::put('booking_data', $booking);
        Session::put('order_review', true);
        return '/order_view';

    }

    public function viewOrder()
    {
        $crumbs = 'Order review';
        if (Session::get('order_review')) {
            $ordered_pizza = $this->requestOrder();
            return view('backend.order.index', compact('crumbs', 'ordered_pizza'));
        } else {
            return redirect('dashboard/pizza');
        }
    }

    public function viewOrderDelete(Request $request)
    {
        $reference = $request->get('ref');
        return view('backend.order._includes._delete_request',compact('reference'));
    }

    public function orderDelete(Request $request)
    {
        try{
            $purchase = Purchase::where('reference_number','=',$request->get('reference'))->firstOrFail();
        }catch(ModelNotFoundException $e){
            return Redirect::back()->with([
                'alert-level' => 'error',
                'message' => 'No data found.'
            ]);
        }
        $purchase->delete();

        return Redirect::back()->with([
            'alert-level' => 'success',
            'message' => 'Invoice #: '. $request->get('reference') .' already deleted.'
        ]);
    }

    public function orders()
    {
        $crumbs = 'Order';

        $reviewOrder = $this->reviewOrder();

        return view('backend.order.view',compact('crumbs','reviewOrder'));
    }

    public function invoiceView(Request $request)
    {
        $ref = $request->get('ref');

        $purchase = $this->reviewOrder($ref);


        return view('backend.invoice.request',compact('purchase'));
    }

    public function requestUpdate(Request $request){
        $reference = $request->get('ref');
        return view('backend.order._includes._update_request',compact('reference'));
    }



    public function orderUpdate(Request $request)
    {
        $purchase = Purchase::with(['userPurchase' => function($query){
            $query->with(['profile']);
        }])->where('reference_number','=', $request->get('reference'))->firstOrFail();
        $purchase->status = $request->get('status');

        if(Auth::user()->isAdmin()){
            $purchase->edited_by = 'administrator';
            $purchase->edited_by_user_id = Auth::user()->id;
        }elseif(Auth::user()->isStoreKeeper()){
            $purchase->edited_by = 'storekeeper';
            $purchase->edited_by_user_id = Auth::user()->id;
        }

        $purchase->save();

        event(new OrderHasConfirmed($purchase,$request->get('status')));

        return redirect()->back()->with([
            'alert-level' => 'success',
            'message' => 'Order confirmed '. $request->get('reference')
        ]);
    }

    private function requestOrder()
    {
        $booking = Session::get('booking_data');
        $pizzaIds = array();
        $pizzaSize = array();

        foreach ($booking as $book) {
            $pizzaIds[] = $book['pizzaId'];
            $pizzaSize[] = $book['pizzaSize'];
            $quantity[$book['pizzaSize']] = $book['quantity'];
        }

        $data = Pizza::with(['pizzaSize' => function ($query) use ($pizzaSize) {
            $query->whereIn('id', $pizzaSize);
        }])->whereIn('id', $pizzaIds)->get();


        foreach($data as $dkey => $value){
            foreach($value->pizzaSize as $vkey => $pricing){
                if(in_array($pricing->id,$pizzaSize)){
                    $data[$dkey]['amount'] = number_format($quantity[$pricing->id] * $pricing->price,2);
                    $data[$dkey]['quantity'] = $quantity[$pricing->id] ;
                }
            }
        }

        return $data;
    }

    public function reviewOrder($ref = 'undefined')
    {
        if($ref == 'undefined'){
            if(Auth::user()->isCustomer()){
                return Purchase::with(['userPurchase' => function($qUser){
                    $qUser->with(['profile']);
                },'purchaseList' => function ($query) {
                    $query->with(['pizzaPricing' => function ($q) {
                        $q->with(['pizzaSize']);
                    }]);
                }])->where('user_id','=',Auth::user()->id)->get();
            }else{
                return Purchase::with(['editedBy' => function($editedBy){
                    $editedBy->with(['profile']);
                },'userPurchase' => function($qUser){
                    $qUser->with(['profile']);
                },'purchaseList' => function ($query) {
                    $query->with(['pizzaPricing' => function ($q) {
                        $q->with(['pizzaSize']);
                    }]);
                }])->get();
            }
        }else{
            return Purchase::with(['userPurchase' => function($qUser){
                $qUser->with(['profile']);
            },'purchaseList' => function ($query) {
                $query->with(['pizzaPricing' => function ($q) {
                    $q->with(['pizzaSize']);
                }]);
            }])->where('reference_number','=', $ref)->get();
        }
    }
}