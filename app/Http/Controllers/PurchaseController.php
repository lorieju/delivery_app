<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreatePurchaseRequest;
use App\Purchase;
use App\PurchaseDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PurchaseController extends Controller {

    /**
     * @param CreatePurchaseRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function create(CreatePurchaseRequest $request){
        $crumbs = 'Invoice';
        $location = $request->get('location');
        $amount = $request->get('amount');
        if (Session::get('order_review')) {
            $purchase = $this->storePurchasePizza($request);
            return view('backend.invoice.index',compact('crumbs','location','amount','purchase'));
        } else {
            return redirect('dashboard/pizza');
        }
    }

    /**
     * @param CreatePurchaseRequest $request
     * @param $location
     * @param $amount
     * @param $total
     * @return \App\Purchase
     */
    public function storePurchasePizza(CreatePurchaseRequest $request)
    {
        Session::forget('order_review');

        $reference_number = Purchase::referenceNumber();

        $purchase = new Purchase([
            'user_id' => Auth::user()->id,
            'location' => $request->get('location'),
            'amount' => $request->get('amount'),
            'total' => $request->get('total'),
            'status' => 'pending',
            'reference_number' => $reference_number
        ]);
        $purchase->save();

        $purchaseDetails = [];
        foreach ($request->get('pizzaSize') as $pk => $id) {
            foreach ($request->get('quantity') as $prk => $value) {
                if($pk == $prk){
                    $purchaseDetails[] = new PurchaseDetail(['pizza_sizes_id' => $id,'quantity' => $value]);
                }
            }
        }
        $purchase->purchaseList()->saveMany($purchaseDetails);

        $purchase_details = $purchase->with(['purchaseList' => function($query){
            $query->with(['pizzaPricing' => function($q){
                $q->with(['pizzaSize']);
            }]);
        }])->findOrFail($purchase->id);

        return $purchase_details;
    }
}
