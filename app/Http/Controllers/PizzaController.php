<?php namespace App\Http\Controllers;

use App\Commands\CreatePizza;
use App\Commands\UpdatePizza;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreatePizzaRequest;
use App\Http\Requests\UpdatePizzaRequest;
use App\Pizza;
use App\PizzaSize;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class PizzaController extends Controller {

	public function index(){
        $crumbs = 'Pizza';
        $Pizzas = Pizza::with(['pizzaSize'])->orderBy('created_at','desc')->paginate(4);
        return view('backend.pizza.index', compact('crumbs','Pizzas'));
    }

    public function manage(){
        $crumbs = 'Pizza Management';
        $pizzas = Pizza::fetchAll();
        return view('backend.pizza.manage',compact('crumbs','pizzas'));
    }

    public function add(){
        $crumbs = 'Add Pizza';
        $pizza = null;
        return view('backend.pizza.add',compact('crumbs','pizza'));
    }

    public function create(CreatePizzaRequest $request)
    {
        $uploadStatus = $this->uploadPicture($request);

        if($uploadStatus['success']){
            $this->dispatch(new CreatePizza($request,$uploadStatus['file_name']));
        }else{
            return redirect()->back()->with([
                'alert-level' => 'error',
                'message' => $uploadStatus['message']
            ]);
        }

        return redirect('dashboard/pizza-management')->with([
            'alert-level' => 'success',
            'message' => 'Pizza successfully added to the menu.'
        ]);

    }

    public function edit($id)
    {
        $crumbs = 'Edit Pizza';

        try{
            $pizza = Pizza::where('id','=',$id)->firstOrFail();
        }catch(ModelNotFoundException $e){
            return Redirect::back()->with([
                'alert-level' => 'error',
                'message' => 'No data found.'
            ]);
        }

        $count = 1;
        return view('backend.pizza.edit',compact('pizza','count','crumbs'));
    }

    public function requestDelete($id)
    {
        try{
            $pizza = Pizza::where('id','=',$id)->firstOrFail();
        }catch(ModelNotFoundException $e){
            return Redirect::back()->with([
                'alert-level' => 'error',
                'message' => 'No data found.'
            ]);
        }
        return view('backend.pizza._request_delete',compact('pizza'));
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        try{
            $pizza = Pizza::where('id','=',$id)->firstOrFail();
        }catch(ModelNotFoundException $e){
            return Redirect::back()->with([
                'alert-level' => 'error',
                'message' => 'No data found.'
            ]);
        }

        $pizza->delete();

        return Redirect::back()->with([
            'alert-level' => 'success',
            'message' => 'Pizza successfully deleted.'
        ]);
    }

    public function update(UpdatePizzaRequest $request)
    {
        if($request->file('image')){
            $uploadStatus = $this->uploadPicture($request,'update');
        }else{
            $uploadStatus['success'] = true;
        }

        if($uploadStatus['success']){
            $this->dispatch(new UpdatePizza($request));
        }else{
            return redirect()->back()->with([
                'alert-level' => 'error',
                'message' => $uploadStatus['message']
            ]);
        }

        return redirect('dashboard/pizza-management')->with([
            'alert-level' => 'success',
            'message' => 'Pizza successfully updated.'
        ]);
    }

    private function uploadPicture(Request $request,$storeType = 'default'){
            // Valid extensions
            $validExtensions = array('jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'bmp', 'BMP', 'gif', 'GIF');

            // Max file size (1mb)
            $maxSize = 1048576;

            // Upload directory
            $requestPath = 'pizza';

            // Set pixel
            $pixelWidth = 400;
            $pixelHeight = 250;
            $path = public_path() . '/img/' . $requestPath . '/';

            // Get the file
            $file = $request->file('image');

            // Get file extension
            $extension = $file->getClientOriginalExtension();

            // Get file size
            $fileSize = $file->getClientSize();

            // Create new file name
            $newFileName = time().(rand(1,999)).'.'.$extension;

            // Create upload path if not exist
            if ( ! file_exists($path)) {
                mkdir($path, 0777);
            }

            // Check file size
            if ($fileSize > $maxSize)
            {
                return [
                    'success' => false,
                    'message' => 'File is too large.'
                ];
            }

            // Check if extension is valid
            if (in_array($extension, $validExtensions))
            {
                // Move uploaded file from temp to uploads directory
                if ($file->move($path, $newFileName))
                {
                    $image = Image::make(sprintf($path.'/%s', $newFileName));

                    $image->crop($pixelWidth, $pixelHeight);
                    $image->resize($pixelWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $image->save();

                    // Update filename to Model
                    if($storeType == 'update'){
                        if($requestPath == 'pizza')
                        {
                            $pizza = Pizza::where('id','=',$request->get('id'))->firstOrFail();
                            $oldImage = $pizza->image;
                            $pizza->image = $newFileName;
                            $pizza->save();
                        }
                        else
                        {
                            return [
                                'success' => false,
                                'message' => 'Unknown error.'
                            ];
                        }

                        // Delete old image
                        File::delete($path.$oldImage);
                    }


                    return [
                        'success' => true,
                        'file_name' => $newFileName
                    ];
                }

                return [
                    'success' => false,
                    'message' => 'Unknown error.'
                ];
            }

            return [
                'success' => false,
                'message' => 'Invalid file extension.'
            ];
    }

}
