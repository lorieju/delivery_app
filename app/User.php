<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email', 'password','active'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','role_user');
    }

    public function isAdmin()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('admin', $roles);
    }

    public function isCustomer()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('customer', $roles);
    }

    public function isStoreKeeper()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('storekeeper', $roles);
    }

    public static function fetchAll()
    {
        return Self::with(['profile','roles'])->where('id','!=',Auth::user()->id)->get();
    }

    public function userPurchase(){
        return $this->belongsToMany('App\Purchase','user_id');
    }

    public function editedBy(){
        return $this->belongsToMany('App\Purchase','edited_by_user_id');
    }

    public static function doUpdate($userId,Request $request)
    {
        $user = User::with(['profile','roles'])->where('id','=',$userId)->first();

        $user->profile->first_name = $request->get('first_name');
        $user->profile->last_name = $request->get('last_name');
        $user->profile->mobile = $request->get('mobile');
        $user->profile->save();

        return $user;
    }
}
