<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model {

    protected $fillable = [
        'purchase_id',
        'pizza_sizes_id',
        'quantity'
    ];

    public function purchaseList(){
        return $this->belongsTo('App\Purchase');
    }

    public function pizzaPricing(){
        return $this->hasMany('App\PizzaSize','id','pizza_sizes_id');
    }

}
