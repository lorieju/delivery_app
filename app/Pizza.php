<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pizza extends Model {

    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'image'
    ];

	public function pizzaSize(){
        return $this->hasMany('App\PizzaSize');
    }

    public static function fetchAll(){
        return Pizza::orderBy('created_at','desc')->get();
    }

}
