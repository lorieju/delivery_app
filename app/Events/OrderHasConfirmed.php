<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class OrderHasConfirmed extends Event {

	use SerializesModels;

    public $reference;
    public $email;
    public $name;
    public $location;
    public $status;
	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($purchase,$status)
	{
		$this->reference = $purchase->reference_number;
		$this->email = $purchase->userPurchase->email;
		$this->name = $purchase->userPurchase->profile->first_name.' '.$purchase->userPurchase->profile->last_name;
		$this->location = $purchase->location;
		$this->status = $status;
	}

}
