<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

    protected $fillable = ['user_id','first_name','last_name','mobile','image'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
