<?php namespace App\Handlers\Events;

use App\Events\OrderHasConfirmed;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class EmailConfirmationNotification {

    protected $mailer;
	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderHasConfirmed  $event
	 * @return void
	 */
	public function handle(OrderHasConfirmed $event)
	{

        $data = [
            'name' => ucwords($event->name),
            'order' => $event->status,
            'reference' => $event->reference
        ];

        $this->mailer->queue('backend.mail.orderConfirm', $data, function($message) use($event)
        {
            $message->to($event->email, ucwords($event->name))
                ->subject('[The Rollin\' Pin] Order Notification');
        });
	}

}
