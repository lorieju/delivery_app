<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PizzaSize extends Model {

    protected $fillable = [
        'size',
        'price'
    ];

	public function pizzaSize(){
        return $this->belongsTo('App\Pizza','pizza_id','id');
    }

    public function pizzaPricing(){
        return $this->belongsTo('App\PurchaseDetail','pizza_sizes_id','id');
    }
}
