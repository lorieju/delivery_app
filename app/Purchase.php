<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model {

    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'location',
        'amount',
        'total',
        'status',
        'edited_by',
        'reference_number'
    ];

    public function purchaseList(){
        return $this->hasMany('App\PurchaseDetail');
    }

    public function userPurchase(){
        return $this->belongsTo('App\User','user_id');
    }

    public function editedBy(){
        return $this->belongsTo('App\User','edited_by_user_id');
    }

    public static function referenceNumber()
    {
        $count = Purchase::all()->count();
        $refID = str_pad(($count + 1),5,0,STR_PAD_LEFT);
        return $refID . '-' . rand(1000,9999);
    }
}
