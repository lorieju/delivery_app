<?php

use App\Profile;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        $user = [
            [
                'email'     => 'admin@gmail.com',
                'password'  => bcrypt('password'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'email'     => 'storekeeper@gmail.com',
                'password'  => bcrypt('password'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'email'     => 'customer@gmail.com',
                'password'  => bcrypt('password'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];
        User::insert($user);
    }

} 