<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchases', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('location');
            $table->float('amount');
            $table->float('total');
            $table->enum('status', ['pending', 'approved','cancelled']);
            $table->string('reference_number')->unique()->index();
            $table->string('edited_by')->nullable();
            $table->integer('edited_by_user_id')->unsigned();
			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('purchases');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
