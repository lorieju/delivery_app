(function() {
    'use strict';

    angular.module('deliveryApp')
        .controller('userController', userController);

    userController.$inject = ['$scope','userService','$location'];

    function userController ($scope, userService, $location){

        var user = this;
        console.log('outside');
        user.logout = function(){
            location.reload();
            console.log('triggered!');
        }
    }

})();