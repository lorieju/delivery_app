(function(){
    'use strict';

    angular.module('deliveryApp')
        .controller('navigationController', navigationController);

    navigationController.$inject = ['$scope','$location'];

    function navigationController($scope,$location){

        $('#side-menu').metisMenu();

        $scope.segmentUrl = function(path){
            var thisUrl = $location.path().split('/dashboard');
            if (thisUrl.pop() === path) {
                return 'active';
            } else {
                return '';
            }
        }
    }

})();