<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:53:24 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>The Rollin' Pin | 404 Error</title>

    <link rel="stylesheet" href="{{ elixir('css/back-style.css') }}">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Page Not Found</h3>

        <div class="error-desc">
            Sorry, but the page you are looking for has note been found. Try checking the URL for error, then hit the refresh button on your browser or try found something else in our app.
            <br />
            <hr />
            <a href="/" class="btn btn-primary btn-lg">Home</a>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script type="text/javascript" src="{{ elixir('js/back-scripts.js') }}"></script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:53:24 GMT -->
</html>
