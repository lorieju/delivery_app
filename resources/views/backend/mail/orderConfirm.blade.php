<p>Hello, {{ $name }}!</p>

<p>
    Thank you for choosing us, this is your invoice number: {{ $reference }}
</p>
    @if($order == 'approved')
        <h2>Your order is already approved by the admin. Please keep in touch on your mobile phone.</h2>
        <h1 style="color:#62e23f;">Status: {{ ucwords($order) }}</h1>
    @else
        <h2>Sorry, your order has been cancelled by the admin. You can try order again.</h2>
        <h1 style="color:#e24162;">Status: {{ ucwords($order) }}</h1>
    @endif
<p>
    We'll get back to you very soon.
</p>

<p>
    Regards,<br/>
    The Rollin' Pin
</p>