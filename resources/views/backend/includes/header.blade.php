<nav class="navbar-default navbar-static-side" role="navigation" ng-controller="navigationController">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{ Auth::user()->profile->image ? '/img/profile/'. Auth::user()->profile->image : '/img/profile_small.jpg' }}" style="max-width: 50px;" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->profile->first_name .' '. Auth::user()->profile->last_name }}</strong>
                             </span>
                             <span class="text-muted text-xs block">
                                @foreach(Auth::user()->roles as $role)
                                    {{ ucfirst($role->name == 'admin' ? 'administrator' : $role->name) }}
                                @endforeach
                             <b class="caret"></b></span>
                             </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ url('/profile') }}">Profile</a></li>
                        <li><a href="{{ url('/dashboard/order') }}">Orders</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    TRP
                </div>
            </li>
            <li class="{{ Active::pattern('dashboard') }}">
                <a href="/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
            </li>
            <li class="{{ Active::pattern('dashboard/order') }}">
                <a href="/dashboard/order"><i class="fa fa-bicycle"></i> <span class="nav-label">Orders</span> </a>
            </li>
            {{--<li>--}}
                {{--<a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">Mailbox </span><span class="label label-warning pull-right">16/24</span></a>--}}
                {{--<ul class="nav nav-second-level">--}}
                    {{--<li><a href="#">Inbox</a></li>--}}
                    {{--<li><a href="#">Email view</a></li>--}}
                    {{--<li><a href="#">Compose email</a></li>--}}
                    {{--<li><a href="#">Email templates</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li class="{{ Active::pattern('dashboard/pizza') }}">
                <a href="/dashboard/pizza"><i class="fa fa-yelp"></i> <span class="nav-label">Pizza</span> </a>
            </li>
            @if(Auth::user()->isAdmin())
                <li class="{{ Active::pattern('dashboard/pizza-management') }}">
                    <a href="/dashboard/pizza-management"><i class="fa fa-yelp"></i> <span class="nav-label">Pizza Management</span> </a>
                </li>
                <li class="{{ Active::pattern('dashboard/user') }}">
                    <a href="/dashboard/user"><i class="fa fa-user"></i> <span class="nav-label">User Management</span> </a>
                </li>
            @endif
        </ul>

    </div>
</nav>