<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><i class="fa fa-users text-success"></i> Users</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ number_format($users) }}</h1>
                    <div class="stat-percent font-bold text-success"><i class="fa fa-circle"></i></div>
                    <small>Total Active Users</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><i class="fa fa-bicycle text-info"></i> Approved Orders</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ number_format($approved) }}</h1>
                    <div class="stat-percent font-bold text-info"><i class="fa fa-circle"></i></div>
                    <small>Total Approved Orders</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><i class="fa fa-bicycle text-warning"></i> Pending Orders</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ number_format($pending) }}</h1>
                    <div class="stat-percent font-bold text-warning"><i class="fa fa-circle"></i></div>
                    <small>Total Pending Orders</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><i class="fa fa-bicycle text-danger"></i> Cancelled Orders</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ number_format($cancelled) }}</h1>
                    <div class="stat-percent font-bold text-danger"><i class="fa fa-circle"></i></div>
                    <small>Total Cancelled Orders</small>
                </div>
            </div>
        </div>
    </div>
</div>