<div class="col-lg-4">
    <div class="ibox">
        <div class="ibox-content">
            <h1>Logo here</h1>
            {{--<img src="" alt="...">--}}
        </div>
    </div>
</div>

<div class="col-lg-8">
    <div class="ibox">
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Welcome to The Rollin' Pin, {{ ucwords(Auth::user()->profile->first_name.' '.Auth::user()->profile->last_name) }}</h3>
                    <hr />
                    <h4>We're delighted to have you on board. To get you familiar with the dashboard.</h4>
                    <h5>Enjoy your stay! <i class="fa fa-angellist"></i></h5>
                </div>
            </div>
        </div>
    </div>
</div>