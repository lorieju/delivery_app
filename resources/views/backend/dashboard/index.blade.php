@extends('backend.index')

@section('content')
<div class="col-md-12 col-sm-12">
    @if(Auth::user()->isCustomer())
        @include('backend.dashboard.customer')
    @else
        @include('backend.dashboard.admin')
    @endif
</div>
@endsection