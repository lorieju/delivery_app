<div class="container-fluid">
    <h3>Change {{ ucwords($user->profile->first_name.' '.$user->profile->last_name) }} Status?</h3>
    <div style="min-height: 80px;margin-top:35px;">
        <div class="col-md-6">
            {!! Form::open(['url' => 'account_type']) !!}
            <input type="hidden" name="status" value="1"/>
            <input type="hidden" name="id" value="{{ $user->id }}"/>
            <button class="btn btn-success text-center"><i class="fa fa-check"></i> Active</button>
            {!! Form::close() !!}
        </div>
        <div class="col-md-6">
            {!! Form::open(['url' => 'account_type']) !!}
            <input type="hidden" name="status" value="0"/>
            <input type="hidden" name="id" value="{{ $user->id }}"/>
            <button class="btn btn-danger"><i class="fa fa-remove"></i> Deactivate</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>