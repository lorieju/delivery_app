@extends('backend.index')

@section('content')
<style>
    .loading_stage{
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #DCDCDC;
        z-index: 99;
    }
</style>
<div class="loading_stage hidden">
    <div class="spiner-example">
        <div class="sk-spinner sk-spinner-double-bounce">
            <div class="sk-double-bounce1"></div>
            <div class="sk-double-bounce2"></div>
        </div>
    </div>
    <h1 class="text-center">Please wait...</h1>
</div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>User Management</h5>
            <div class="ibox-tools">
                <a href="{{ url('/dashboard/add_user') }}" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> User</a>
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

        <table class="table table-striped table-bordered table-hover dataTables-user" >
        <thead>
        <tr>
            <th>Name</th>
            <th>Account Type</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Status</th>
            <th class="text-center"> <i class="fa fa-wrench"></i></th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ ucwords($user->profile->first_name .' '. $user->profile->last_name) }}</td>
            @foreach($user->roles as $role)
                <td>{{ ucwords($role->name) }}</td>
            @endforeach
            <td>{{ $user->email }}</td>
            <td>{{ $user->profile->mobile }}</td>
            <td>{!! $user->active == 1? '<label class="label label-primary" >Active</label>' : '<label class="label label-danger" >Deactivated</label>' !!}</td>
            <td class="text-center"><a href="#" class="btn btn-sm btn-danger user_type" id="{{ $user->id }}"><i class="fa fa-lock"></i></a></td>
        </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>Name</th>
            <th>Account Type</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Status</th>
            <th class="text-center"> <i class="fa fa-wrench"></i></th>
        </tr>
        </tfoot>
        </table>

        </div>
    </div>
        <script>
            $(document).ready(function() {
                $('.dataTables-user').dataTable({
                    responsive: true
                });

                var modalTitle = $('.modal-title');
                var modalSmall = $('.modal-small');
                var modalContentSmall = $('.modal-content-small');

                $('.user_type').on('click',function(e){
                    e.preventDefault();
                    var id = $(this).attr('id');
                    $('.loading_stage').removeClass('hidden');
                    $.ajax({
                        url: '/account_type',
                        type: 'GET',
                        data: {'_token': $('meta[name="_token"]').attr('content'),'user': id}
                    })
                    .always(function(result) {
                        modalTitle.html('<i class="fa fa-gavel"></i> Make Action');
                        modalContentSmall.html(result);
                        modalSmall.modal('show');
                        $('.loading_stage').addClass('hidden');
                    });
                });
            });
        </script>
@endsection