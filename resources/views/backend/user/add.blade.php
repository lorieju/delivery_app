@extends('backend.index')

@section('content')
<div class="ibox-content p-xl">
    <section class="content-header">
        <a href="{{ url('dashboard/user') }}" class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back</a>

        <h1>{{ ucwords($crumbs) }}</h1>
    </section>

    <section class="content">

        <div class="box">

            <div class="box-body">
                <div class="row">
                {!! Form::open(['url' => 'add_user', 'class' => 'on-submit-disable']) !!}
                    <div id="profile" class="col-md-9 col-md-offset-3 col-sm-12">
                            <h3>Add user information</h3>

                            <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First Name">
                                {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name">
                                {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('mobile') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile">
                                {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                            </div>

                            <h3>Account Type</h3>

                            <div class="form-group" style="width: 50%;">
                              <select name="type" class="form-control">
                                <option value="3">Customer</option>
                                <option value="2">StoreKeeper</option>
                                <option value="1">Administrator</option>
                              </select>
                            </div>

                            <h3>Password</h3>

                            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="password" class="form-control" placeholder="Password"/>
                                {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password"/>
                                {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group" style="width: 50%;">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-plus"></i> Add</button>
                                    </div>
                                </div><br />
                                <div class="alert alert-info">
                                    <p>*Please take note of the <strong>password</strong>.</p>
                                </div>
                            </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </section>
</div>
@endsection