<!DOCTYPE html>
<html ng-app="deliveryApp">
<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/dashboard_4_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:52:00 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="_token" content="{{ csrf_token() }}" />
    <title>{{ empty($crumbs) ? 'Dashboard' : ucfirst($crumbs) }} | The Rollin' Pin</title>
    <link rel="stylesheet" href="{{ elixir('css/back-style.css') }}">
    <script src="/js/jquery-1.11.0.min.js"></script>
</head>

<body>
    <div id="wrapper">
        @include('backend.includes.header')
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Welcome to The Rollin' Pin</span>
                        </li>
                        <li>
                            <a href="{{ url('/auth/logout') }}">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>{{ empty($crumbs) ? 'Dashboard' : ucfirst($crumbs) }}</h2>
                </div>
            </div>
            <div class="wrapper wrapper-content">
                <section class="content-header">
                    @if (session('message'))
                        @include('backend.flash.template')
                    @endif
                </section>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="animated fadeInRightBig">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.includes.footer')
        </div>
    </div>
    @include('backend.modals.index')
    <script type="text/javascript" src="{{ elixir('js/back-scripts.js') }}"></script>
    <script type="text/javascript" src="{{ elixir('js/backend-script.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });
        });
    </script>
</body>
<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/dashboard_4_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:52:00 GMT -->
</html>
