@extends('backend.index')

@section('content')
<div class="ibox-content p-xl">
    <section class="content-header">
        <a href="{{ url('profile') }}" class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back</a>

        <h1>{{ ucwords($crumbs) }}</h1>
    </section>

    <section class="content">

        <div class="box">

            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#" class="thumbnail">
                          <img alt="image" class="img-circle" src="{{ Auth::user()->profile->image ? '/img/profile/'. Auth::user()->profile->image : '/img/profile_small.jpg' }}" style="max-width: 200px;" />
                        </a>
                    </div>
                    <div id="profile" class="col-md-9 col-sm-12">

                        {!! Form::open(['url' => 'upload', 'files' => 'true']) !!}
                            <h3>Edit profile picture</h3>

                            <span class="btn btn-default btn-file" style="position: relative;">
                                <i class="fa fa-image"></i> Browse {!! Form::file('image') !!}
                                 <div class="message" style="width: 92%;height: 54px;position: absolute;top: 0; display:none;color:#000000;background-color:#F8F359;"></div>
                            </span>

                            {!! Form::hidden('path', 'profile') !!}
                            {!! Form::hidden('id', Auth::id()) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['url' => 'profile/update', 'class' => 'on-submit-disable']) !!}
                            <hr/>
                            <h3>Edit your details below</h3>

                            <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="text" class="form-control" name="first_name" value="{{ ucwords(Auth::user()->profile->first_name) }}" placeholder="First Name">
                                {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="text" class="form-control" name="last_name" value="{{ ucwords(Auth::user()->profile->last_name) }}" placeholder="Last Name">
                                {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('mobile') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="number" class="form-control" name="mobile" value="{{ Auth::user()->profile->mobile }}" placeholder="Mobile">
                                {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group" style="width: 50%;">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-check"></i> Done</button>
                                        </div>
                                </div>
                            </div>
                        {!! Form::close() !!}

                        {!! Form::open(['url' => 'profile/change_password', 'class' => 'on-submit-disable']) !!}
                            <hr/>
                            <h3>Change your password</h3>

                            <div class="form-group has-feedback {{ $errors->has('current_password') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="current_password" class="form-control" placeholder="Current password"/>
                                {!! $errors->first('current_password', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('new_password') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="new_password" class="form-control" placeholder="Password"/>
                                {!! $errors->first('new_password', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="new_password_confirmation" class="form-control" placeholder="Retype password"/>
                                {!! $errors->first('new_password_confirmation', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group" style="width: 50%;">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-lock"></i> Change</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>
    <script>
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        $('.btn-file').addClass('disabled');
        var msg = $('.message');
        msg.show();
        msg.removeClass('text-danger').html('<h2><i class="fa fa-spin fa-spinner"></i> Please wait.</h2>');
        var form = $(this).closest('form');
        var formData = new FormData();
        formData.append('file', $(this).prop('files')[0]);
        formData.append('_token', form.find('input[name=_token]').val());
        formData.append('path', form.find('input[name=path]').val());
        formData.append('id', form.find('input[name=id]').val() || null);

        var url = form.attr('action');


        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false
        })
            .always(function(result) {
                if (result.success)
                {
                    msg.removeClass('text-danger');
                    msg.addClass('text-success').html(result.message);

                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
                else
                {
                    msg.removeClass('text-success');
                    msg.addClass('text-danger').html(result.message);
                }

                $('.btn-file').removeClass('disabled');
            });
    });
    </script>

@endsection