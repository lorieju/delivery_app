@extends('backend.index')

@section('content')
<div class="ibox-content p-xl">
    <section class="content-header">
        <a href="{{ url('profile/edit') }}" class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-pencil"></i> Edit</a>

        <h1>{{ ucwords($crumbs) }}</h1>
    </section>

    <section class="content">

        <div class="box">

            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#" class="thumbnail">
                          <img class="img-circle" src="{{ Auth::user()->profile->image ? '/img/profile/'. Auth::user()->profile->image : 'img/profile_small.jpg' }}" alt="..." style="max-width: 200px;" />
                        </a>
                        <div class="well">
                            @foreach(Auth::user()->roles as $role)
                                Account type: <label class="label label-success">{{ ucfirst($role->name == 'admin' ? 'administrator' : $role->name) }}</label>
                            @endforeach
                        </div>
                    </div>

                    <div id="profile" class="col-md-9 col-sm-12">

                        <div class="labels">
                            <h1>{{ ucwords(Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name) }}</h1>
                            <h5><i class="fa fa-envelope"></i> &nbsp; {{ Auth::user()->email }}</h5>
                            <h5><i class="fa fa-phone"></i> &nbsp;&nbsp; {{ Auth::user()->profile->mobile }}</h5>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </section>
</div>
@endsection