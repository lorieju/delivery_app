@extends('backend.index')

@section('content')
<style>
 .takeOrder{
     background-color: #fff;
     border: 3px solid #23c6c8;
     color: #23c6c8;
 }
</style>
    @if($Pizzas->count() < 1)
        <div class="col-lg-12">
            <div class="ibox" style="opacity: 1; z-index: 0;">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <h2>
                        No available pizza yet.
                    </h2>
                </div>
            </div>
        </div>
    @else
    {!! Form::open(['url' => '/booking', 'id' => 'orderPizza' ,'class' => 'on-submit-disable']) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="pull-right">
        <a href="#" class="btn btn-lg btn-info takeOrder">Take an order now!</a>
    </div>
    <div class="clearfix"></div>
     <hr />
    @foreach(array_chunk($Pizzas->all(), 2) as $pizzas)
        @foreach($pizzas as $pizza)
            <div class="col-lg-6 pizzaList">
                    <div class="ibox" style="opacity: 1; z-index: 0;">
                        <div class="ibox-title">
                            <h5>Pizza</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                        <div class="checkbox i-checks pull-right"><label><i class="fa fa-shopping-cart"></i> Buy me <input type="checkbox" name="selectedOrder" class="selectedOrder" value="{{ $pizza->id }}"></label></div>
                            <h2>
                                {{ ucfirst($pizza->name) }}
                            </h2>
                            <p>
                                <img src="/img/pizza/{{ $pizza->image }}" style="max-width: 400px; max-height: 250px;" />
                            </p>
                            <p>
                                {!! BBCode::parse($pizza->description) !!}
                            </p>
                            <hr />
                            <h4>Choose pizza size</h4>
                            <div class="radio_container_{{ $pizza->id }}">
                            @foreach($pizza->pizzaSize as $pricing)
                                    <div class="radio i-checks">
                                        <label> <input type="radio" value="{{ $pricing->id }}" class="pricing" name="pricing_{{ $pizza->id }}"> <i></i> Inch: {{ $pricing->size .'\'\'  | Price: Php '. number_format($pricing->price,2) }} </label>
                                    </div>
                            @endforeach
                            <div class="form-group">
                              <label>Quantity</label>
                              <input type="number" class="pizza_qnty" name="qauntity[]" placeholder="0" value="1" />
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
        <div class="clearfix"></div>
    @endforeach
    {!! Form::close() !!}
    {!! $Pizzas->render() !!}

    @endif

    <script>
        $(document).ready(function () {
            var ordered = $('.selectedOrder');

            var takeOrder_btn = $('.takeOrder');

            takeOrder_btn.on('click',function(){
            var flag = 1;
            var pizzaSelected = [];
                var submitForm = false;
                ordered.each(function(e,v){
                    if($(this).is(':checked')){
                        var id = $(this).val();
                        var radio_container = $('.radio_container_'+ id);
                        var selected = radio_container.find('.radio label').find('.checked');
                        if(selected.length < 1){
                            setTimeout(function() {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                    showMethod: 'slideDown',
                                    timeOut: 4000
                                };
                                toastr.error('Please choose pizza size.');

                            }, 1300);
                        }else{
                          flag = 2;
                          var qnty = radio_container.find('.pizza_qnty');
                          if(qnty.val() < 1){
                            submitForm = false;
                            setTimeout(function() {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                    showMethod: 'slideDown',
                                    timeOut: 4000
                                };
                                toastr.error('Minimum of one quantity.');

                            }, 1300);
                          }else{
                            submitForm = true;
                            pizzaSelected.push({'pizzaSize' : selected.find('input').val(),'pizzaId': id,'quantity': qnty.val()});
                          }
                        }
                    }else{
//                    console.log(flag);
//                        if(flag == 1){
//                            setTimeout(function() {
//                                toastr.options = {
//                                    closeButton: true,
//                                    progressBar: true,
//                                    showMethod: 'slideDown',
//                                    timeOut: 4000
//                                };
//                                toastr.error('Which pizza did you select?');
//
//                            }, 1300);
//                            flag++;
//                        }
                    }
                });

                if(submitForm){
                    var form = $("#orderPizza");
                    $.ajax({
                            url: form.attr('action'),
                            type: 'POST',
                            data: {'_token': form.find('input[name=_token]').val(), 'data' : pizzaSelected}
                    })
                    .always(function(result) {
                        window.location.replace(result);
                    });
                }
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });
        });
    </script>
@endsection