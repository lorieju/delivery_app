<div class="col-md-12" style="margin-bottom:30px;">
    <div class="col-md-6">
            @if($errors->first('image'))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            @if(str_contains($error, 'image'))
                            <li>{{ $error }}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            @endif

            <h3>Insert picture for this pizza</h3>

            <span class="btn btn-default btn-file" style="position: relative;">
                <i class="fa fa-image"></i> Browse {!! Form::file('image') !!}
                 <div class="message" style="width: 92%;height: 54px;position: absolute;top: 0; display:none;color:#000000;background-color:#F8F359;"></div>
            </span>

            @if($pizza)
                <br />
                <div style="margin-top:10px;">
                    <img src="{{ url('/img/pizza/'.$pizza->image) }}" style="max-width: 300px;max-height: 200px;" class="well" />
                </div>
            @endif
    </div>
    <div class="col-md-6">
    @if($errors->first('size[]') && $errors->first('price[]'))
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                <li>Please fill-up pricing field[s].</li>
            </ul>
        </div>
    @endif
        <div id='TextBoxesGroup'>
            <h3>Pizza pricing</h3>
            @if($pizza != null)
                @foreach($pizza->pizzaSize as $pricing)
                <div id="TextBoxDiv{{ $count++ }}" class="sizeCount">
                    <div class="col-md-6"  style="margin-bottom: 20px;">
                        <label>Size : </label>
                        <input type="number" name="size[]" placeholder=" Inch" id="textbox" value="{{ $pricing->size }}" required />
                    </div>
                    <div class="col-md-6"  style="margin-bottom: 20px;">
                        <label>Price : </label>
                        <input type="number" name="price[]" placeholder=" Price" id="textbox" value="{{ $pricing->price }}" required />
                    </div>
                </div>
                @endforeach
            @else
             <div id="TextBoxDiv1">
                <div class="col-md-6"  style="margin-bottom: 20px;">
                    <label>Size : </label>
                    <input type="number" name="size[]" placeholder=" Inch" id="textbox" value="" required />
                </div>
                <div class="col-md-6"  style="margin-bottom: 20px;">
                    <label>Price : </label>
                    <input type="number" name="price[]" placeholder=" Price" id="textbox" value="" required />
                </div>
            </div>
            @endif
        </div>
        <div>
            <p>Add size and price to your pizza</p>
            <a href="#" class="btn btn-primary" id='addButton'><i class="fa fa-plus"></i></a>
            <a href="#" class="btn btn-danger" id='removeButton'><i class="fa fa-remove"></i></a>
        </div>
    </div>
</div>
<br />
<h3>Let's add some details</h3>

<div class="form-group has-feedback {{ $errors->has('title') ? 'has-error' : '' }}">
    <input type="text" class="form-control" name="title" value="{{ $pizza ? ucwords($pizza->name) : old('title') }}" placeholder="Pizza title here">
    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group has-feedback {{ $errors->has('description') ? 'has-error' : '' }}">
    <textarea name="description" id="editor" rows="12" placeholder="Add some description.">{{ $pizza ? ucwords($pizza->description) : old('description') }}</textarea>
    {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group" style="width: 50%;">
    <div class="row">
        <div class="col-sm-12">
        @if($pizza != null)
            <input type="hidden" name="id" value="{{ $pizza->id }}" />
        @endif
            <button type="submit" id="pizza_btn" class="btn btn-lg btn-success btn-flat pull-right"><i class="fa fa-check"></i> Submit</button>
            </div>
    </div>
</div>
