@extends('backend.index')

@section('content')
<div class="ibox-content p-xl">
    <section class="content-header">
        <a href="{{ url('dashboard/pizza-management') }}" class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back</a>

        <h1>{{ ucwords($crumbs) }}</h1>
    </section>

    <section class="content">

        <div class="box">

            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    {!! Form::open(['url' => 'pizza', 'method' => 'PUT', 'class' => 'on-submit-disable','files' => 'true']) !!}
                        @include('backend.pizza._form')
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>
    <script>
    $(function() {
        $("#editor").wysibb();
    });

    $(document).ready(function(){

        var counter = $('.sizeCount').length;
        $("#addButton").click(function () {
    	if(counter>5){
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.error('Only 5 size allowed.');

            }, 1300);
            return false;
    	}

    	var newTextBoxDiv = $(document.createElement('div'))
    	     .attr("id", 'TextBoxDiv' + counter);

    	newTextBoxDiv.after().html('<div class="col-md-6"  style="margin-bottom: 20px;">' +
            '<label>Size : </label>' +
                 '<input type="number" name="size[]" placeholder=" Inch" id="textbox' + counter + '" value="" required />' +
          '</div>' +
          '<div class="col-md-6"  style="margin-bottom: 20px;">' +
               '<label>Price : </label>' +
                 '<input type="number" name="price[]" placeholder=" Price" id="textbox' + counter + '" value="" required />' +
             '</div>');

    	newTextBoxDiv.appendTo("#TextBoxesGroup");


    	counter++;
         });

         $("#removeButton").click(function () {
    	if(counter==0){
              setTimeout(function() {
                  toastr.options = {
                      closeButton: true,
                      progressBar: true,
                      showMethod: 'slideDown',
                      timeOut: 4000
                  };
                  toastr.error('No more textbox to remove.');

              }, 1300);
              return false;
           }

        $("#TextBoxDiv" + counter).remove();
        counter--;

         });

         $('#pizza_btn').on('click',function(e){
            if(counter < 1){
                setTimeout(function() {
                      toastr.options = {
                          closeButton: true,
                          progressBar: true,
                          showMethod: 'slideDown',
                          timeOut: 4000
                      };
                      toastr.error('Please add pizza pricing.');

                  }, 1300);
                e.preventDefault();
            }
         });
      });
    </script>

@endsection