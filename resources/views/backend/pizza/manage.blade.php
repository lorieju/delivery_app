@extends('backend.index')

@section('content')
    <style>
        .loading_stage{
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: #DCDCDC;
            z-index: 99;
        }
    </style>
    <div class="loading_stage hidden">
        <div class="spiner-example">
            <div class="sk-spinner sk-spinner-double-bounce">
                <div class="sk-double-bounce1"></div>
                <div class="sk-double-bounce2"></div>
            </div>
        </div>
        <h1 class="text-center">Please wait...</h1>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Pizza Manager</h5>
            <div class="ibox-tools">
                <a href="{{ url('dashboard/pizza-add') }}" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Add Pizza</a>
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

        <table class="table table-striped table-bordered table-hover dataTables-pizza" >
        <thead>
        <tr>
            <th>Image</th>
            <th>Title</th>
            <th>Description</th>
            <th>Pricing</th>
            <th>Created Date</th>
            <th class="text-center" style="min-width: 70px;"> <i class="fa fa-wrench"></i></th>
        </tr>
        </thead>
        <tbody>
        @foreach($pizzas as $pizza)
            <tr>
                <td><img style="max-width: 50px;max-height: 50px;" src="/img/pizza/{{ $pizza->image }}" /></td>
                <td>{{ $pizza->name }}</td>
                {{--<td>{!! BBCode::parse($pizza->description) !!}</td>--}}
                <td>{!! BBCode::parse(str_limit($pizza->description, 70)) !!}</td>
                <td>
                    @foreach($pizza->pizzaSize as $pricing)
                        <p>Inch: {{ $pricing->size .'\'\'  | Price: Php '. number_format($pricing->price,2) }}</p>
                    @endforeach
                </td>
                <td>{{ $pizza->created_at->diffForHumans() }}</td>
                <td class="text-center">
                    <p>
                        <a href="{{ url('/dashboard/pizza-edit/'.$pizza->id) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a> <a href="{{ url('/dashboard/pizza-delete/'.$pizza->id) }}" class="btn btn-sm btn-danger pizza_delete"><i class="fa fa-trash"></i></a>
                    </p>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>Image</th>
            <th>Title</th>
            <th>Description</th>
            <th>Pricing</th>
            <th>Created Date</th>
            <th class="text-center"> <i class="fa fa-wrench"></i></th>
        </tr>
        </tfoot>
        </table>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.dataTables-pizza').dataTable({
                responsive: true
            });

            var modalTitle = $('.modal-title');
            var modalSmall = $('.modal-small');
            var modalContentSmall = $('.modal-content-small');

            $('.pizza_delete').on('click',function(e){
                e.preventDefault();
                var url = $(this).attr('href');
                $('.loading_stage').removeClass('hidden');
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {'_token': $('meta[name="_token"]').attr('content')}
                })
                .always(function(result) {
                    modalTitle.html('<i class="fa fa-trash"></i> Delete');
                    modalContentSmall.html(result);
                    modalSmall.modal('show');
                    $('.loading_stage').addClass('hidden');
                });
            });
        });
    </script>
@endsection