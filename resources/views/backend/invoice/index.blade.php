@extends('backend.index')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content p-xl">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>From:</h5>
                            <address>
                                <strong>{{ Auth::user()->profile->first_name .' '. Auth::user()->profile->last_name }}</strong><br>
                                {{ $purchase->location }}<br>
                                Silliman<br>
                                <abbr title="Phone"><i class="fa fa-mobile"></i> Mobile:</abbr> {{ Auth::user()->profile->mobile }}
                            </address>
                        </div>

                        <div class="col-sm-6 text-right">
                            <h4>Invoice No.</h4>
                            <h4 class="text-navy">{{ $purchase->reference_number }}</h4>
                            <span>To:</span>
                            <address>
                                <strong>The rollin pin</strong><br>
                                location here<br>
                                Silliman<br>
                                <abbr title="Phone"><i class="fa fa-mobile"></i> Mobile:</abbr> (+63) 9000-4321
                            </address>
                            <p>
                                <span><strong>Invoice Date:</strong> {{ $purchase->created_at->format('d M Y - h:i:s a') }}</span><br/>
                            </p>
                        </div>
                    </div>

                    <div class="table-responsive m-t">
                        <table class="table invoice-table">
                            <thead>
                            <tr>
                                <th>Item List</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Total Price</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($purchase->purchaseList as $userPurchase)
                            <tr>
                                @foreach($userPurchase->pizzaPricing as $list)
                                    @foreach($userPurchase->pizzaPricing as $list)
                                        <td>
                                            <div>
                                            <img src="/img/pizza/{{ $list->pizzaSize->image }}" style="max-width: 50px; max-height: 50px;"/><strong>
                                            {{ ucwords($list->pizzaSize->name) }}</strong>
                                            </div>
                                            <small>Description: {{ ucfirst($list->pizzaSize->description) }}</small><br />
                                            <small>Inch: {{ ucfirst($list->size) }}''</small>
                                        </td>
                                        <td>{{ $userPurchase->quantity }}</td>
                                        <td>Php {{ number_format($list->price,2) }}</td>
                                        <td>Php {{ number_format(($list->price * $userPurchase->quantity),2) }}</td>
                                    @endforeach
                                @endforeach
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /table-responsive -->
                    <hr />
                    <table class="table invoice-total">
                        <tbody>
                        <tr>
                            <td><strong>TOTAL :</strong></td>
                            <td>Php {{ number_format($purchase->total,2) }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="text-right">
                        <a href="{{ url('/dashboard/order') }}" class="btn btn-lg btn-primary"><i class="fa fa-check"></i> Close</a>
                    </div>

                    <div class="well m-t"><strong>Confirmation: </strong>
                        Please check your email for the confirmation of your order.
                    </div>
                </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('Thank you purchasing our products.');

        }, 1300);
    });
</script>
@endsection