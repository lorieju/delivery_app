<div class="container-fluid">
    <table class="table table-hover" >
    <thead>
    <tr>
        <th>Image</th>
        <th>Title</th>
        <th>Description</th>
        <th>Location</th>
        <th>Size</th>
        <th>Quantity</th>
        <th>Unit Price</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($purchase as $data)
        @foreach($data->purchaseList as $value)
        <tr>
            @foreach($value->pizzaPricing as $v)
                @if($v->pizzaSize != null)
                    <td><img style="max-width: 60px;max-height: 60px;" src="/img/pizza/{{ $v->pizzaSize->image }}"></td>
                    <td>{{ $v->pizzaSize->name }}</td>
                    <td>{!! BBCode::parse(str_limit(ucfirst($v->pizzaSize->description), 70)) !!}</td>
                    <td>{{ ucwords($data->location) }}</td>
                    <td>{{ $v->size }}''</td>
                    <td>{{ $value->quantity }}</td>
                    <td>Php {{ number_format($v->price,2) }}</td>
                    <td>Php {{ number_format(($v->price * $value->quantity),2) }}</td>
                @else
                    <td colspan="5"><h3>Pizza has been removed.</h3></td>
                @endif
            @endforeach
         </tr>
        @endforeach
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3"><strong>Total: </strong>Php {{ number_format($data->total,2) }}</td>
        </tr>
    @endforeach
    </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>