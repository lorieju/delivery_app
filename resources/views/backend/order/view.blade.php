@extends('backend.index')

@section('content')
<style>
    .loading_stage{
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #DCDCDC;
        z-index: 99;
    }
</style>
<div class="loading_stage hidden">
    <div class="spiner-example">
        <div class="sk-spinner sk-spinner-double-bounce">
            <div class="sk-double-bounce1"></div>
            <div class="sk-double-bounce2"></div>
        </div>
    </div>
    <h1 class="text-center">Please wait...</h1>
</div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
        @if(Auth::user()->isCustomer())
            @include('backend.order._includes.customer')
        @else
            @include('backend.order._includes.admin')
        @endif


        </div>
    </div>
        <script>
            $(document).ready(function() {
                $('.dataTables-order').dataTable({
                    responsive: true
                });
            });
        </script>
@endsection