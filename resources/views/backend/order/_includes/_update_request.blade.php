<div class="container-fluid">
    <h3>Order Status?</h3>
    <div style="min-height: 80px;margin-top:35px;">
        <div class="col-md-6">
            {!! Form::open(['url' => 'order_update']) !!}
            <input type="hidden" name="status" value="approved"/>
            <input type="hidden" name="reference" value="{{ $reference }}"/>
            <button class="btn btn-success text-center"><i class="fa fa-check"></i> Approved</button>
            {!! Form::close() !!}
        </div>
        <div class="col-md-6">
            {!! Form::open(['url' => 'order_update']) !!}
            <input type="hidden" name="status" value="cancelled"/>
            <input type="hidden" name="reference" value="{{ $reference }}"/>
            <button class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>