<table class="table table-striped table-bordered table-hover dataTables-order" >
<thead>
<tr>
    <th>Invoice No.</th>
    <th>Amount</th>
    <th>Total Purchase</th>
    <th>Change</th>
    <th>Status</th>
    <th>Date</th>
</tr>
</thead>
<tbody>
@foreach($reviewOrder as $userPurchase)
<tr>
    <td>
        <a href="#" class="invoice_view" id="{{ $userPurchase->reference_number }}">{{ $userPurchase->reference_number }}</a>
    </td>
    <td>Php {{ number_format($userPurchase->amount,2) }}</td>
    <td>Php {{ number_format($userPurchase->total,2) }}</td>
    <td>Php {{ number_format(($userPurchase->amount - $userPurchase->total),2) }}</td>
    @if($userPurchase->status == 'pending')
        <td><label class="label label-warning">{{ $userPurchase->status }}</label></td>
    @else
        <td><label class="label label-success">{{ $userPurchase->status }}</label></td>
    @endif
    <td>{{ $userPurchase->created_at->format('d M Y - h:i:s a') }}</td>
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
    <th>Invoice No.</th>
    <th>Amount</th>
    <th>Total Purchase</th>
    <th>Change</th>
    <th>Status</th>
    <th>Date</th>
</tr>
</tfoot>
</table>
<script>
    $(document).ready(function(){
        var viewInvoice = $('.invoice_view');

        var modalTitle = $('.modal-title');
        var modalLarge = $('.modal-large');
        var modalContentLarge = $('.modal-content-large');

        viewInvoice.on('click', function(e) {
            var $this = $(this);
            var id = $this.attr('id');
            var url = '/view_invoice';

            $('.loading_stage').removeClass('hidden');
            $.ajax({
                url: url,
                type: 'GET',
                data: {'_token': $('meta[name="_token"]').attr('content'), 'ref' : id}
            })
            .always(function(result) {
                modalTitle.html('View Invoice: ' + id);
                modalContentLarge.html(result);
                modalLarge.modal('show');
                $('.loading_stage').addClass('hidden');
            });

            e.preventDefault();
        });
    });
</script>