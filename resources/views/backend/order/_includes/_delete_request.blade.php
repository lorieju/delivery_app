<div class="container-fluid">
    <h3>Are you sure to delete Invoice #: {{ $reference }}?</h3>
    <div style="min-height: 80px;margin-top:35px;">
        <div class="col-md-6">
            {!! Form::open(['url' => 'order_delete']) !!}
            <input type="hidden" name="reference" value="{{ $reference }}"/>
            <button class="btn btn-danger text-center"><i class="fa fa-trash"></i> Delete</button>
            {!! Form::close() !!}
        </div>
        <div class="col-md-6">
            <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancel</button>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>