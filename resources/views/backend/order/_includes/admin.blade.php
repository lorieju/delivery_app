<table class="table table-striped table-bordered table-hover dataTables-order" >
<thead>
<tr>
    <th>Invoice No.</th>
    <th>Name</th>
    <th>Mobile</th>
    <th>Amount</th>
    <th>Total Purchase</th>
    <th>Change</th>
    <th>Status</th>
    <th>Date</th>
    <th>Edited By</th>
    @if(!Auth::user()->isCustomer())
    <th class="text-center"> <i class="fa fa-wrench"></i></th>
    @endif
</tr>
</thead>
<tbody>
@foreach($reviewOrder as $userPurchase)
 {{--{{ dd($userPurchase->editedBy) }}--}}
<tr>
    <td>
        <a href="#" class="invoice_view" id="{{ $userPurchase->reference_number }}">{{ $userPurchase->reference_number }}</a>
    </td>
    <td>{{ $userPurchase->userPurchase->profile->first_name.' '.$userPurchase->userPurchase->profile->last_name }}</td>
    <td>{{ $userPurchase->userPurchase->profile->mobile }}</td>
    <td>Php {{ number_format($userPurchase->amount,2) }}</td>
    <td>Php {{ number_format($userPurchase->total,2) }}</td>
    <td>Php {{ number_format(($userPurchase->amount - $userPurchase->total),2) }}</td>
    @if($userPurchase->status == 'pending')
        <td><label class="label label-warning">{{ $userPurchase->status }}</label></td>
    @elseif($userPurchase->status == 'approved')
        <td><label class="label label-success">{{ $userPurchase->status }}</label></td>
    @else
        <td><label class="label label-danger">{{ $userPurchase->status }}</label></td>
    @endif
    <td>{{ $userPurchase->created_at->format('d M Y - h:i:s a') }}</td>
    @if($userPurchase->editedBy == null)
        <td><label class="label label-primary">New</label></td>
    @else
        <td><label class="label label-primary">{{ $userPurchase->edited_by }}</label><br />
            {{ ucwords($userPurchase->editedBy->profile->first_name.' '.$userPurchase->editedBy->profile->last_name) }}
        </td>
    @endif

    @if(!Auth::user()->isCustomer())
    <td class="text-center">
        <p>
            <a href="#" id="{{ $userPurchase->reference_number }}" class="btn btn-sm btn-success order_status"><i class="fa fa-pencil"></i></a>
             @if(Auth::user()->isAdmin())
            <a href="#" class="btn btn-sm btn-danger order_delete" id="{{ $userPurchase->reference_number }}"><i class="fa fa-trash"></i></a>
            @endif
        </p>
    </td>
    @endif
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
    <th>Invoice No.</th>
    <th>Name</th>
    <th>Mobile</th>
    <th>Amount</th>
    <th>Total Purchase</th>
    <th>Change</th>
    <th>Status</th>
    <th>Date</th>
    <th>Edited By</th>
    @if(!Auth::user()->isCustomer())
    <th class="text-center" style="min-width: 70px;"> <i class="fa fa-wrench"></i></th>
    @endif
</tr>
</tfoot>
</table>
<script>
    $(document).ready(function(){
        var viewInvoice = $('.invoice_view');
        viewInvoice.on('click',function(){


        });

        var modalTitle = $('.modal-title');
        var modalLarge = $('.modal-large');
        var modalContentLarge = $('.modal-content-large');

        var modalSmall = $('.modal-small');
        var modalContentSmall = $('.modal-content-small');

        viewInvoice.on('click', function(e) {
            var $this = $(this);
            var id = $this.attr('id');
            var url = '/view_invoice';

            $('.loading_stage').removeClass('hidden');
            $.ajax({
                url: url,
                type: 'GET',
                data: {'_token': $('meta[name="_token"]').attr('content'), 'ref' : id}
            })
            .always(function(result) {
                modalTitle.html('View Invoice: ' + id);
                modalContentLarge.html(result);
                modalLarge.modal('show');
                $('.loading_stage').addClass('hidden');
            });

            e.preventDefault();
        });

        $('.order_status').on('click',function(e){
            e.preventDefault();
            var ref = $(this).attr('id');
            $('.loading_stage').removeClass('hidden');
            $.ajax({
                url: '/order_update',
                type: 'GET',
                data: {'_token': $('meta[name="_token"]').attr('content'),'ref': ref}
            })
            .always(function(result) {
                modalTitle.html('<i class="fa fa-gavel"></i> Make Action <br />Invoice:' + ref );
                modalContentSmall.html(result);
                modalSmall.modal('show');
                $('.loading_stage').addClass('hidden');
            });
        });

        $('.order_delete').on('click',function(e){
            e.preventDefault();
            var ref = $(this).attr('id');
            $('.loading_stage').removeClass('hidden');
            $.ajax({
                url: '/order_delete',
                type: 'GET',
                data: {'_token': $('meta[name="_token"]').attr('content'),'ref': ref}
            })
            .always(function(result) {
                modalTitle.html('<i class="fa fa-gavel"></i> Make Action <br />Invoice:' + ref );
                modalContentSmall.html(result);
                modalSmall.modal('show');
                $('.loading_stage').addClass('hidden');
            });
        });


    });
</script>