@extends('backend.index')

@section('content')
<style>
    .orderList-title{
        padding: 10px;
        position: relative;
        background-color: #f8ac59;
        text-align: center;
    }
</style>
<div class="col-md-12 col-sm-12">
{!! Form::open(['url' => 'purchase']) !!}
    <div class="col-lg-4">
        <div class="ibox">
            <div class="ibox-content">
                <h3><i class="fa fa-archive"></i> Review your order <a href="{{ url('dashboard/pizza') }}" class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back</a></h3>
                <br />
                <p>*You can remove your specific order by clicking the 'x' <label class="label label-danger">button</label></p>
                <ul class="sortable-list connectList agile-list">
                    @foreach($ordered_pizza as $order)
                    <li class="warning-element orderList_{{ $order->id }}" style="padding-top: 0px;padding-left: 10px;">
                        <div class="orderList-title">
                        {{ ucwords(str_limit($order->name,18)) }}
                        <a href="#" class="pull-right btn btn-xs btn-danger remove_order" id="{{ $order->id }}"><i class="fa fa-remove"></i></a>
                        </div>
                        <div style="margin-top: 10px;">
                            <img src="/img/pizza/{{ $order->image }}" class="img-thumbnail" alt="..."  style="max-height: 70px;max-width:70px;"/>
                            <i class="fa fa-arrows-v"></i><span class="pricing"> Inch <span class="p_inch">{{ $order->pizzaSize[0]->size }}'' </span></span>|
                            <i class="fa fa-money"></i><span class="pricing"> Php <span class="p_price">{{ number_format($order->pizzaSize[0]->price,2)}}</span></span>
                        </div>
                        <br />
                        {!! BBCode::parse(str_limit($order->description, 70)) !!}
                        <br />
                        <br />
                        <div class="agile-detail" style="display: inline;">
                            <h5 class="qnty_display">Quantity: <span class="pizza_qnty">{{ $order->quantity }}</span></h5>
                            <input type="number" name="quantity[]" class="pizza_qnty_npt hidden" value="{{ $order->quantity }}">
                            <h5>Total: Php <span class="list_total">{{ $order->amount }}</span></h5>
                            <div class="pull-right">
                                <a href="#" id="{{ $order->id }}" class="btn btn-xs btn-primary edit_order"><i class="fa fa-pencil"></i></a>
                                <a href="#" id="{{ $order->id }}" class="btn btn-xs btn-primary save_order hidden"><i class="fa fa-check"></i></a>
                            </div>
                        </div>
                        <br />
                        <input type="hidden" name="pizza[]" value="{{ $order->id }}" />
                        <input type="hidden" name="pizzaSize[]" value="{{ $order->pizzaSize[0]->id }}" />
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="col-lg-8">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-b-md">
                            <h2><i class="fa fa-user"></i> Customer Details</h2>
                            <img alt="image" class="img-circle pull-right" style="max-width: 80px;max-height: 80px;position: absolute;right: 5px;top:0;border:3px solid #e2e2e2;" src="{{ Auth::user()->profile->image ? '/img/profile/'. Auth::user()->profile->image : '/img/profile_small.jpg' }}" />
                        </div>
                        <dl class="dl-horizontal">
                            <dt>Status:</dt> <dd><span class="label label-primary">Active</span></dd>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <dl class="dl-horizontal">

                            <dt><i class="fa fa-tag"></i> Costumer Name:</dt> <dd>{{ Auth::user()->profile->first_name .' '. Auth::user()->profile->last_name }}</dd>
                            <dt><i class="fa fa-envelope-o"></i> Email:</dt> <dd>{{ Auth::user()->email }}</dd>
                            <dt><i class="fa fa-phone"></i> Mobile:</dt> <dd>{{ Auth::user()->profile->mobile }}</dd>
                            <dt><i class="fa fa-bicycle"></i> Delivery:</dt> <dd><div class="form-group has-feedback {{ $errors->has('location') ? 'has-error' : '' }}">
                                                                                     <input type="text" class="form-control" name="location" value="{{ old('location') }}" placeholder="Location within Silliman" required />
                                                                                     {!! $errors->first('location', '<span class="help-block">:message</span>') !!}
                                                                                 </div></dd>
                            <p>*For us to give the exact change if necessary.</p>
                            <dt><i class="fa fa-money"></i> Amount on hand:</dt> <dd><div class="form-group has-feedback {{ $errors->has('amount') ? 'has-error' : '' }}">
                                                                                      <input type="number" class="form-control" id="inpt_amount" name="amount" value="{{ old('amount') }}" placeholder="00.00" required/>
                                                                                      {!! $errors->first('amount', '<span class="help-block">:message</span>') !!}
                                                                                  </div></dd>
                        </dl>
                    </div>
                </div>
                <div class="pull-right">
                    <h3>Total: Php <span id="orderTotal">00.00</span></h3>
                    <input type="hidden" name="total" id="total_inpt" value="" />
                </div><br />
                <hr />
                <button type="submit" class="btn btn-lg btn-primary form-control submit_order"><i class="fa fa-shopping-cart"></i> Submit Order</button>
                <br />
                <p class="pull-right">
                    <a href="{{ url('dashboard/pizza') }}" class="btn btn-danger">Cancel this order</a>
                </p>
                <p>*Delivery service only offers within Silliman area.</p>
                <p>*Please make sure the details is correct.</p>

            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<script>
$(document).ready(function(){
    var price = $('.list_total');
    var total = 0;
    var orderTotal = $('#orderTotal');
    var total_inpt = $('#total_inpt');
    price.each(function(e,v){
        total += parseFloat($(this).html().replace(/\s/g, "").replace(",", ""));
    });

    orderTotal.html(ReplaceNumberWithCommas(total.toFixed(2)));
    total_inpt.attr('value',total.toFixed(2));
    $('.submit_order').on('click',function(e){
        if(parseInt($('#inpt_amount').val()) < total && $('#inpt_amount').val() != ''){
            setTimeout(function() {
                  toastr.options = {
                      closeButton: true,
                      progressBar: true,
                      showMethod: 'slideDown',
                      timeOut: 4000
                  };
                  toastr.error('Please check your amount.');

            }, 1300);
            e.preventDefault();
        }
    });

    $('.remove_order').on('click',function(){
        var listId = $(this).attr('id');
        var price = $(this).parent().parent().find('.list_total');
        total = total -  parseFloat(price.html().replace(/\s/g, "").replace(",", ""));
        orderTotal.html(ReplaceNumberWithCommas(total.toFixed(2)));
        total_inpt.attr('value',total.toFixed(2));
        $('.orderList_' + listId).remove();
        if(total == 0){
            $('.submit_order').on('click',function(e){
                e.preventDefault();
                setTimeout(function() {
                      toastr.options = {
                          closeButton: true,
                          progressBar: true,
                          showMethod: 'slideDown',
                          timeOut: 4000
                      };
                      toastr.error('Do you want to cancel your order?');

                }, 1300);
            });
        }
    });

    $('.edit_order').on('click',function(){
        var edit = $(this);
        var id = edit.attr('id');
        edit.hide();
        var save = edit.parent().find('.save_order');
        save.removeClass('hidden');
        var pizza_list = $('.orderList_' + id);
        pizza_list.find('.qnty_display').hide();
        var qnty_npt = $('.orderList_' + id).find('.pizza_qnty_npt');
        qnty_npt.removeClass('hidden');
        save.on('click',function(){
            if(qnty_npt.val() < 1){
                setTimeout(function() {
                      toastr.options = {
                          closeButton: true,
                          progressBar: true,
                          showMethod: 'slideDown',
                          timeOut: 4000
                      };
                      toastr.error('Minimum of one quantity.');

                }, 1300);
            }else{
                edit.show();
                save.addClass('hidden');
                qnty_npt.addClass('hidden');
                var price = pizza_list.find('.p_price').text();
                pizza_list.find('.qnty_display').show();
                pizza_list.find('.pizza_qnty').html(qnty_npt.val());
                var sum = parseInt(qnty_npt.val()) *  parseFloat(price.replace(/\s/g, "").replace(",", ""));
                pizza_list.find('.list_total').html(ReplaceNumberWithCommas(sum.toFixed(2)));
                total = 0;
                $('.list_total').each(function(e,v){
                    total += parseFloat($(this).html().replace(/\s/g, "").replace(",", ""));
                });

                orderTotal.html(ReplaceNumberWithCommas(total.toFixed(2)));
                total_inpt.attr('value',total.toFixed(2));
            }
        });
    });

    function ReplaceNumberWithCommas(yourNumber) {
        //Seperates the components of the number
        var n= yourNumber.toString().split(".");
        //Comma-fies the first part
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //Combines the two sections
        return n.join(".");
    }

});
</script>
@endsection