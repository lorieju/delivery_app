<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <p>Copyright &copy; The Rollin' Pin Your Website 2015</p>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="{{ elixir('js/lib.js') }}"></script>
{{--<script type="text/javascript" src="{{ elixir('js/front-scripts.js') }}"></script>--}}

<!-- Script to Activate the Carousel -->
<script>
$('.carousel').carousel({
    interval: 5000 //changes the speed
})
</script>