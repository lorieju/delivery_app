<!DOCTYPE html>
<html ng-app="deliveryApp">
<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/dashboard_4_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:52:00 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="/dashboard">
    <title>The Rollin' Pin</title>
    <link rel="stylesheet" href="{{ elixir('css/back-style.css') }}">
</head>

<body>
    <div id="wrapper" ng-controller="userController as user">
        <div ng-include src="'/views/backend/includes/navs.html'"></div>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Welcome to The Rollin' Pin</span>
                        </li>
                        <li>
                            <a href="{{ url('/auth/logout') }}" ng-click="user.logout()">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Dashboard</h2>
                </div>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div ng-view></div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> The Rollin' Pin &copy; 2015
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function(){
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('Welcome to The Rollin\' Pin');

        }, 1300);
    });
    </script>
    <script type="text/javascript" src="{{ elixir('js/back-scripts.js') }}"></script>
    <script type="text/javascript" src="{{ elixir('js/backend-script.js') }}"></script>
</body>
<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/dashboard_4_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:52:00 GMT -->
</html>
