<!-- Header -->
<header class="text-center" name="home">
  <div class="intro-text">
    <h1 class="wow fadeInDown">The Rol<strong><span class="color">lin' Pin</span></strong></h1>
    <p class="wow fadeInDown">Coffee - Pastry - Snack</p>
    <p style="margin-bottom: 10px;"><a href="{{ url('/auth/login') }}" class="btn btn-default btn-lg page-scroll wow fadeInUp" data-wow-delay="200ms" style="width: 25%;"><i class="fa fa-sign-in"></i> Login</a></p>
    <p><a href="{{ url('/auth/register') }}" class="btn btn-default btn-lg page-scroll wow fadeInUp" data-wow-delay="200ms" style="width: 25%;"><i class="fa fa-pencil"></i> Register</a></p>
  </div>
</header>