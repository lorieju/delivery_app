@extends('index')

@section('content')

@include('frontend.header')

@include('frontend.service')

@include('frontend.food')

@include('frontend.about')

@include('frontend.team')

@include('frontend.testimonials')

@include('frontend.contact')

@include('frontend.footer')

@stop