@foreach($pizzas as $pizza)
<div class="portfolio-modal modal fade" id="pizza_{{ $pizza->id }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"> </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body">
            <!-- Project Details Go Here -->
            <h2>{{ ucwords($pizza->name) }}</h2>
            <img class="img-responsive img-centered" src="/img/pizza/{{ $pizza->image }}" alt="..">
            <p>{!! BBCode::parse($pizza->description) !!}</p>
            <ul class="list-inline">
            @foreach($pizza->pizzaSize as $pricing)
                <li style="display: list-item;"><strong>Inch:</strong> {!! $pricing->size .'\'\'  | <strong>Price:</strong> Php '. number_format($pricing->price,2) !!}</li>
            @endforeach
            </ul>
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach