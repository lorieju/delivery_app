<h2 id="title">{{ $pizza->name }}</h2>
{{--<p class="item-intro">{!! BBCode::parse($pizza->description) !!}</p>--}}
<img class="img-responsive img-centered" src="/img/pizza/{{ $pizza->image }}" alt="...">
<p class="item-intro">{!! BBCode::parse($pizza->description) !!}</p>
<ul class="list-inline">
  <li><strong>Client</strong>: John Doe</li>
  <li><strong>Category</strong>: Web Design</li>
</ul>
<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>