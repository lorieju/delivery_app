<!-- Portfolio Section -->
{{--<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
<div id="works-section" class="text-center">
  <div class="container"> <!-- Container -->
    <div class="section-title wow fadeInDown">
      <h2>Our <strong>Good Food</strong></h2>
      <hr>
      <div class="clearfix"></div>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p>
    </div>
    <div class="categories">
      <div class="clearfix"></div>
    </div>
    <div class="row">
      <div class="portfolio-items">
      @foreach(array_chunk($pizzas->all(),4) as $pizzas)
        @foreach($pizzas as $pizza)
            <div class="col-sm-6 col-md-3 col-lg-3">
              <div class="portfolio-item wow fadeInUp" data-wow-delay="200ms">
                <div class="hover-bg"> <a href="#pizza_{{ $pizza->id }}" class="portfolio-link pizzaItem" id="{{ $pizza->id }}" data-toggle="modal">
                {{--<div class="hover-bg"> <a href="#" class="portfolio-link pizzaItem" id="{{ $pizza->id }}" data-toggle="modal">--}}
                  <div class="hover-text">
                    <h4>{{ $pizza->name }}</h4>
                    @foreach($pizza->pizzaSize as $pricing)
                        <p>Inch {{ $pricing->size .' Php '. number_format($pricing->price,2) }}</p>
                    @endforeach
                    <div class="clearfix"></div>
                    <i class="fa fa-plus"></i> </div>
                  <img src="/img/pizza/{{ $pizza->image }}" class="img-responsive" alt="{{ $pizza->name }}"> </a> </div>
              </div>
            </div>
        @endforeach
        <div class="clearfix"></div>
       @endforeach
      </div>
    </div>
  </div>
</div>
