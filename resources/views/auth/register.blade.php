@extends('auth.index')

@section('content')
<div class="text-center" style="background-color: #f1c40f; margin-top: 50px; padding: 50px 0;">
  <div class="container">
  <div class="pull-right">
    @if (Auth::guest())
        <a class="btn btn-default" style="background-color: #FCF8E3" href="{{ url('/auth/login') }}">Login</a>
     @else
         <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
             <ul class="dropdown-menu" role="menu">
                 <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
             </ul>
         </li>
     @endif
  </div>
  <br />
    <div class="section-title wow fadeInDown">
      <h2><strong>Register</strong></h2>
      <hr>
      <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 team wow fadeInUp" data-wow-delay="800ms">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                    <label class="col-md-3 control-label">First Name</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                    </div>
                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                    <label class="col-md-3 control-label">Last Name</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                    </div>
                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                    <label class="col-md-3 control-label">Mobile</label>
                    <div class="col-md-6">
                        <input type="number" class="form-control" name="mobile" placeholder="09*********" value="{{ old('mobile') }}">
                    </div>
                    {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-md-3 control-label">E-Mail Address</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="email" placeholder="email@su.edu.ph" value="{{ old('email') }}">
                    </div>
                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="col-md-3 control-label">Password</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label class="col-md-3 control-label">Confirm Password</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-5">
                        <button type="submit" class="btn btn-default" style="background-color: #FCF8E3">
                            Register
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
  </div>
</div>
@endsection
