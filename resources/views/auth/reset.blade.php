@extends('auth.index')

@section('content')
<div class="text-center" style="background-color: #f1c40f; margin-top: 50px; padding: 50px 0;">
  <div class="container">
  <div class="pull-right">
    @if (Auth::guest())
        <a class="btn btn-default" style="background-color: #FCF8E3" href="{{ url('/auth/login') }}">Login</a>
        <a class="btn btn-default" style="background-color: #FCF8E3" href="{{ url('/auth/register') }}">Register</a>
     @else
         <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
             <ul class="dropdown-menu" role="menu">
                 <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
             </ul>
         </li>
     @endif
  </div>
  <br />
    <div class="section-title wow fadeInDown">
      <h2>Reset <strong>Password</strong></h2>
      <hr>
      <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 team wow fadeInUp" data-wow-delay="800ms">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <label class="col-md-4 control-label">E-Mail Address</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Password</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Confirm Password</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Reset Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection

