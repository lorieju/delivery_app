@extends('auth.index')

@section('content')
<div class="text-center" style="background-color: #f1c40f; margin-top: 50px; padding: 50px 0;">
  <div class="container">
  <div class="pull-right">
    @if (Auth::guest())
        <a class="btn btn-default" style="background-color: #FCF8E3" href="{{ url('/auth/register') }}">Register</a>
     @else
         <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
             <ul class="dropdown-menu" role="menu">
                 <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
             </ul>
         </li>
     @endif
  </div>
  <br />
    <div class="section-title wow fadeInDown">
      <h2>Please <strong>Login</strong></h2>
      <hr>
      <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 team wow fadeInUp" data-wow-delay="800ms">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="col-md-4 control-label">E-Mail Address</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Password</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-default" style="background-color: #FCF8E3">Login</button>
                        <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection
