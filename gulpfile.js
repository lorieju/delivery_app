var elixir = require('laravel-elixir');

elixir(function(mix) {

    mix
        .styles([
            'lib/bootstrap.min.css'
        ], 'public/css/lib.css')
        .styles([
            'backend/bootstrap.min.css',
            'frontend/font-awesome/css/font-awesome.css',
            'backend/plugins/toastr/toastr.min.css',
            'backend/plugins/dataTables/dataTables.bootstrap.css',
            'backend/plugins/dataTables/dataTables.responsive.css',
            'backend/plugins/dataTables/dataTables.tableTools.min.css',
            'backend/plugins/morris/morris-0.4.3.min.css',
            'backend/plugins/wysibb/wbbtheme.css',
            'backend/plugins/iCheck/custom.css',
            'backend/animate.css',
            'backend/style.css',
            'backend/myStyle.css'
        ], 'public/css/back-style.css')
        .styles([
            'frontend/font-awesome/css/font-awesome.css',
            'frontend/owl.carousel.css',
            'frontend/owl.theme.css',
            'frontend/style.css',
            'frontend/animate.min.css'
        ], 'public/css/front-style.css');

    mix
    .scripts([
        'lib/jquery.min.js',
        'lib/bootstrap.min.js'
    ],'public/js/lib.js')
    .scripts([
        'frontend/modernizr.custom.js',
        'frontend/jquery.1.11.1.js',
        'frontend/SmoothScroll.js',
        'frontend/wow.min.js',
        'frontend/jquery.prettyPhoto.js',
        'frontend/jquery.isotope.js',
        'frontend/jqBootstrapValidation.js',
        'frontend/owl.carousel.js',
        'frontend/main.js'
    ],'public/js/front-scripts.js')
    .scripts([
        'backend/jquery-2.1.1.js',
        'backend/jquery-1.11.0.min.js',
        'backend/bootstrap.min.js',
        'backend/plugins/metisMenu/jquery.metisMenu.js',
        'backend/plugins/slimscroll/jquery.slimscroll.min.js',
        'backend/plugins/dataTables/jquery.dataTables.js',
        'backend/plugins/dataTables/dataTables.bootstrap.js',
        'backend/plugins/dataTables/dataTables.responsive.js',
        'backend/plugins/dataTables/dataTables.tableTools.min.js',
        'backend/plugins/flot/jquery.flot.js',
        'backend/plugins/flot/jquery.flot.tooltip.min.js',
        'backend/plugins/flot/jquery.flot.spline.js',
        'backend/plugins/flot/jquery.flot.resize.js',
        'backend/plugins/flot/jquery.flot.pie.js',
        'backend/plugins/flot/jquery.flot.symbol.js',
        'backend/plugins/flot/curvedLines.js',
        'backend/plugins/peity/jquery.peity.min.js',
        'backend/inspinia.js',
        'backend/plugins/pace/pace.min.js',
        'backend/plugins/jquery-ui/jquery-ui.min.js',
        'backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'backend/plugins/sparkline/jquery.sparkline.min.js',
        'backend/plugins/chartJs/Chart.min.js',
        'backend/plugins/toastr/toastr.min.js',
        'backend/plugins/wysibb/jquery.wysibb.min.js',
        'backend/plugins/iCheck/icheck.min.js',
        'lib/angular.min.js',
    ],'public/js/back-scripts.js')
    .scripts([
        'angular/main.js',
        'angular/controllers/UserController.js',
        'angular/controllers/NavigationController.js',
        'angular/directives/AdminDirective.js',
        'angular/routes/route.js',
        'angular/services/AdminService.js'
    ],'public/js/backend-script.js');

    mix
    .version([
        'public/css/lib.css',
        'public/css/back-style.css',
        'public/css/front-style.css',
        'public/js/lib.js',
        'public/js/front-scripts.js',
        'public/js/back-scripts.js',
        'public/js/backend-script.js'
    ]);
});